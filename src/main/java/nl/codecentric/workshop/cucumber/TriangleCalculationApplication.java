package nl.codecentric.workshop.cucumber;


/**
 * Contract describing on abstract level to connect to triangle calculation application
 */
public interface TriangleCalculationApplication {

    /**
     * Prepare for triangle calculation
     */
    void prepareForCalculation();


    /**
     * Give input for triangle calculation
     * @param side1
     * @param side2
     * @param side3
     */
    void doTriangleCalculation(final int side1, final int side2, final int side3);


    String getCalculatedTriangleType();

}
